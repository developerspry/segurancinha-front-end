import { ErrorHandler } from './error-handler';
import { Router } from '@angular/router';
import { HttpCustomError } from './model/http-custom-error.model';
import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpHeaders, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from './../../../environments/environment';
import { AuthenticationService } from './../authentication/authentication.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/toPromise';
import { AlertCenterService } from '../shared/components/alert-center/service/alert-center.service';

@Injectable()
export class HttpCustomInterceptor implements HttpInterceptor {

    private authService: AuthenticationService;
    private alertCenterService: AlertCenterService;
    private router: Router;

    constructor(private inj: Injector, private toastyService: ToastyService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.authService = this.inj.get(AuthenticationService);
        this.alertCenterService = this.inj.get(AlertCenterService);

        if (req.url.search('/assets/i18n/') == -1) {
            let urlFinal: string = `${environment.api_url}${req.url}`;

            if (this.authService.isLoggedIn()) {
                const token: string = this.authService.getLoggedUser().token;
                const myRequest = req.clone({
                    setHeaders: { 'x-auth-token': `${token}` },
                    url: urlFinal
                })

                return next.handle(myRequest).catch(error => ErrorHandler.handlerError(error, this.alertCenterService, this.router));
            }

            const myRequest = req.clone({
                url: urlFinal
            })
            return next.handle(myRequest).catch(error => ErrorHandler.handlerError(error, this.alertCenterService, this.router));
        }
        return next.handle(req).catch(error => ErrorHandler.handlerError(error, this.alertCenterService, this.router));
    }
}