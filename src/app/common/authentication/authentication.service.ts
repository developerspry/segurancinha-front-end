import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { LoggedUser } from './../authentication/logged-user';
import { environment } from './../../../environments/environment';

declare var require: any;

const KEY_LOGGED_USER = 'app-logged-user';

@Injectable()
export class AuthenticationService {

    private isLoggedSubject = new ReplaySubject<boolean>(1);
    public isLogado = this.isLoggedSubject.asObservable();

    constructor(private httpClient: HttpClient, private toastyService: ToastyService) { }

    getLoggedUser(): LoggedUser {
        const resp = sessionStorage.getItem(KEY_LOGGED_USER);

        if (resp && resp !== 'undefined' && resp !== 'null') {
            return <LoggedUser>JSON.parse(resp);
        }

        return null;
    }

    setLoggedUser(loggedUser: LoggedUser) {
        if (loggedUser) {
            this.isLoggedSubject.next(true);
            if (loggedUser.roles) {
                loggedUser.roles.push('ROLE_ADMIN');
            } else {
                loggedUser.roles = ['ROLE_ADMIN'];
            } sessionStorage.setItem(KEY_LOGGED_USER, JSON.stringify(loggedUser));
        }
    }

    removeLoggedUser() {
        sessionStorage.removeItem(KEY_LOGGED_USER);
    }


    authentication(login: string, password: string) {
        const params = new HttpParams()
            .set('login', login)
            .set('password', password)
            .set('action', 'LOGIN')
            .set('ip', <any>require('quick-local-ip').getLocalIP4());

        const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

        return this.httpClient.post<LoggedUser>(`${environment.url_authentication}/user`, '',
            {
                params: params,
                headers: headers,
                observe: 'response'
            }).toPromise();
    }

    hasPermission(role: string): boolean {
        return this.getLoggedUser().roles.indexOf(role) !== -1;
    }

    isLoggedIn() {
        return this.getLoggedUser() != null;
    }

    logout() {
        this.removeLoggedUser();
        this.isLoggedSubject.next(false);
    }

}
