export class LoggedUser {
    id: number;
    login: string;
    name: string;
    roles: string[];
    token?:string;
    profileType: string;
}
