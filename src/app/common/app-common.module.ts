import { ErrorValidationService } from './shared/externals/forms/field-error/service/error-validation.service';
import { ExternalModule } from './shared/externals/external.module';
import { ComponentsModule } from './shared/components/components.module';
import { DirectivesModule } from './shared/directives/directives.module';
import { HttpCustomInterceptor } from './http-interceptor/http-custom.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from './authentication/authentication.service';
import { FormsModule } from '@angular/forms';
import { ClickOutsideModule } from 'ng-click-outside';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
import { AgmCoreModule } from '@agm/core';
import { TagInputModule } from 'ngx-chips';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ToastyModule } from 'ng2-toasty';
import { PaginationModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        NgbModule.forRoot(),
        PaginationModule.forRoot(),
        ToastyModule,
        SimpleNotificationsModule.forRoot(),
        TagInputModule,
        AgmCoreModule.forRoot({ apiKey: 'AIzaSyCE0nvTeHBsiQIrbpMVTe489_O5mwyqofk' }),
        Ng2GoogleChartsModule,
        ClickOutsideModule,
        ComponentsModule,
        DirectivesModule,
        ExternalModule,
        TranslateModule
    ],
    declarations: [

    ],
    exports: [
        CommonModule,
        RouterModule,
        FormsModule,
        NgbModule,
        PaginationModule,
        ToastyModule,
        SimpleNotificationsModule,
        TagInputModule,
        AgmCoreModule,
        Ng2GoogleChartsModule,
        ClickOutsideModule,
        ComponentsModule,
        DirectivesModule,
        ExternalModule,
        TranslateModule
    ]
})
export class AppCommonModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: AppCommonModule,
            providers: [
                AuthenticationService,
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: HttpCustomInterceptor,
                    multi: true
                },
                ErrorValidationService
            ]
        }
    }
}
