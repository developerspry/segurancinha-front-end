import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { AuthenticationService } from './../authentication/authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router,private authService: AuthenticationService) { }

    public canActivate(
        route: ActivatedRouteSnapshot, 
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this.authService.isLoggedIn()) {
            return true;
        } else {
            this.router.navigate(['authentication/login']);
            return false;
        }

    }

}