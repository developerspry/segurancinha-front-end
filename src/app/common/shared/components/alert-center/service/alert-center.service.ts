import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {Alert} from '../model/alert';
import {AlertType} from '../model/alert-type';

@Injectable()
export class AlertCenterService {

  // apenas usado para criar uma instância
  alertModel: Alert = new Alert (AlertType.SUCCESS, "", "", 5000);

  private _alerts = new Subject<Alert>();

  get alerts(): Observable<Alert> {
    return this._alerts;
  }

  constructor() {}

  public alertSuccess(title:string, text: string, autoDismissTime: number = 5000, dismissable:boolean = true) {
  
    this.alertModel = new Alert (AlertType.SUCCESS, text, title, autoDismissTime, dismissable);
    this.distpatchEvent(this.alertModel);

  }

  public alertDanger(title:string, text: string, autoDismissTime: number = 5000, dismissable:boolean = true) {
  
    this.alertModel = new Alert (AlertType.DANGER, text, title, autoDismissTime, dismissable);
    this.distpatchEvent(this.alertModel);

  }

  public alertInfo(title:string, text: string, autoDismissTime: number = 5000, dismissable:boolean = true) {
  
    this.alertModel = new Alert (AlertType.INFO, text, title, autoDismissTime, dismissable);
    this.distpatchEvent(this.alertModel);

  }

  public alertWarning(title:string, text: string, autoDismissTime: number = 5000, dismissable:boolean = true) {
  
    this.alertModel = new Alert (AlertType.WARNING, text, title, autoDismissTime, dismissable);
    this.distpatchEvent(this.alertModel);

  }

  distpatchEvent(alert:Alert){
    //console.log('sending alert: ' + JSON.stringify(alert));
    this._alerts.next(alert);
  }

}
