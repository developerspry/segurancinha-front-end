import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Alert} from '../model/alert';
import {AlertType} from '../model/alert-type';

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html'
})

export class AlertComponent implements OnInit {

  @Input() alert = new Alert(AlertType.INFO, '', '');

  @Input() htmlTextEnabled = false;

  @Output() dismissed = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.initTimerIfNeeded();
  }

  isSuccess() {
    return this.alert.alertType === AlertType.SUCCESS;
  }

  isInfo() {
    return this.alert.alertType === AlertType.INFO;
  }

  isWarning() {
    return this.alert.alertType === AlertType.WARNING;
  }

  isDanger() {
    return this.alert.alertType === AlertType.DANGER;
  }

  dismiss() {
    this.dismissed.emit();
  }

  isDismissEnabled() {
    return this.alert.isDismissable();
  }

  isTextStrongDefined() {
    return this.alert.textStrong && this.alert.textStrong.length > 0;
  }

  private initTimerIfNeeded() {
    if (this.alert.isAutoDismissing()) {
      setTimeout(() => this.dismiss(), this.alert.autoDismissTime);
    }
  }
}
