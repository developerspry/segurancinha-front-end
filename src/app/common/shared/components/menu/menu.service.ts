import { AuthenticationService } from './../../../authentication/authentication.service';
import { Menu, ChildrenItems, ItemsMenu, MainMenuItem } from './menu.model';
import { Injectable } from '@angular/core';
import { MENUITEMS } from './menu.data';

@Injectable()
export class MenuService {

  constructor(private authService: AuthenticationService) { }

  getAll(): Menu[] {
    return MENUITEMS;
  }

  getMenu(): Menu[] {
    let menu: Menu[] = [];
    MENUITEMS.forEach(menuItem => {
      menuItem.childrens = (<MainMenuItem[]>this.childrenFilter(menuItem.childrens));
      if (menuItem.childrens.length > 0) menu.push(menuItem);
    });
    return menu;
  }

  childrenFilter(childremItems: ItemsMenu[]) {
    return childremItems.filter(item => {
      if (item.type == 'main' || item.type == 'link') {
        return true;
      } else {
        return false;
      }
    });
  }

}
