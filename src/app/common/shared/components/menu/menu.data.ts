import { Menu } from './menu.model';

export const MENUITEMS: Menu[] = [
    {
        id: 'admin',
        label: 'menu.admin',
        type: 'main',
        childrens: [
            {
                id: 'users-list',
                state: 'users',
                name: 'menu.users',
                type: 'link',
                icon: 'icofont icofont-users',
            }
        ]
    }
];