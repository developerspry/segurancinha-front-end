export interface BadgeItem {
    type: string;
    value: string;
    permission?: string;
}

export interface ChildrenItems extends ItemsMenu {
    state: string;
    target?: boolean;
    name: string;
}

export interface MainMenuItem extends ItemsMenu {
    state: string;
    main_state?: string;
    target?: boolean;
    name: string;
    icon: string;
    badge?: BadgeItem[];
}

export interface Menu extends ItemsMenu {
    label: string;
    childrens: MainMenuItem[];
}

export interface ItemsMenu {
    id: string,
    childrens?: any[];
    type: 'sub' | 'link' | 'external' | 'main';
}