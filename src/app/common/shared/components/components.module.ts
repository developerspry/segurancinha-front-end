import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../app-common.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BreadcrumbsComponent } from './breadcrumbs/breadcrumbs.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { TitleComponent } from './title/title.component';
import { MenuService } from './menu/menu.service';
import { AlertConfirmationDeleteComponent } from './alert-confirmation-delete/alert-confirmation-delete.component';
import { NgxSmartModalModule, NgxSmartModalService } from 'ngx-smart-modal';
import { AlertConfirmationPasswordComponent } from './alert-confirmation-password/alert-confirmation-password.component';
import { ExternalModule } from '../externals/external.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        ExternalModule,
        RouterModule,
        FormsModule,
        TranslateModule,
        NgxSmartModalModule.forRoot()
    ],
    exports: [
        BreadcrumbsComponent,
        SpinnerComponent,
        TitleComponent,
        AlertConfirmationDeleteComponent,
        AlertConfirmationPasswordComponent
    ],
    declarations: [
        BreadcrumbsComponent,
        SpinnerComponent,
        TitleComponent,
        AlertConfirmationDeleteComponent,
        AlertConfirmationPasswordComponent
    ],
    providers: [
        MenuService,
        NgxSmartModalService
    ]
})
export class ComponentsModule { }