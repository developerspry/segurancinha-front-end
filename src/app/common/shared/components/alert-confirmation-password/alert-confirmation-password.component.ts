import { Component, Output, Input, EventEmitter } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { LoggedUser } from '../../../authentication/logged-user';
import { AuthenticationService } from '../../../authentication/authentication.service';

@Component({
  selector: 'alert-confirmation-password',
  templateUrl: './alert-confirmation-password.component.html',
  styleUrls: ['./alert-confirmation-password.component.scss']
})
export class AlertConfirmationPasswordComponent {

  @Input() title: string = "";
  @Input() body: string = "";
  @Input() textButtonClose: string = "";
  @Input() textButtonConfirmation: string = "";

  @Output() eventConfirmUpdate = new EventEmitter();

  userConfirm = {
    password: ''
  }

  loggedUser: LoggedUser = new LoggedUser;

  constructor(
    private authService: AuthenticationService,
    public ngxSmartModalService: NgxSmartModalService) { }

  openModal(loggedUser: LoggedUser) {
    this.loggedUser = loggedUser;
    this.ngxSmartModalService.getModal('modalPassword').open();
  }

  confirm() {
        
    this.ngxSmartModalService.getModal('modalPassword').close();

    this.authService.authentication(this.loggedUser.login, this.userConfirm.password)
      .then(resp => {
        this.eventConfirmUpdate.emit(true);
      }).catch(error => {
        this.eventConfirmUpdate.emit(false);
      });
  }

}
