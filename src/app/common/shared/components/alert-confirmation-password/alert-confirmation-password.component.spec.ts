import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertConfirmationPasswordComponent } from './alert-confirmation-password.component';

describe('AlertConfirmationPasswordComponent', () => {
  let component: AlertConfirmationPasswordComponent;
  let fixture: ComponentFixture<AlertConfirmationPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertConfirmationPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertConfirmationPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
