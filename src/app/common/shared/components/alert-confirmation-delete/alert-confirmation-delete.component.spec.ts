import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertConfirmationDeleteComponent } from './alert-confirmation-delete.component';

describe('AlertConfirmationDeleteComponent', () => {
  let component: AlertConfirmationDeleteComponent;
  let fixture: ComponentFixture<AlertConfirmationDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertConfirmationDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertConfirmationDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
