import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'alert-confirmation-delete',
  templateUrl: './alert-confirmation-delete.component.html',
  styleUrls: ['./alert-confirmation-delete.component.scss']
})
export class AlertConfirmationDeleteComponent implements OnInit {

  @Input() title:string = "";
  @Input() body:string = "";
  @Input() textButtonClose:string = "";
  @Input() textButtonConfirmation:string = "";

  @Output() eventConfirmDelete = new EventEmitter();

  item: any;

  constructor(public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
  }

  openModal(item: any){
    this.item = item;
    this.ngxSmartModalService.getModal('deleteModal').open();
  }

  confirm(){
    // Modal de detalhes do objeto
    this.ngxSmartModalService.getModal('modalDetail').close();

    // Modal de confirmação do delete
    this.ngxSmartModalService.getModal('deleteModal').close();
    this.eventConfirmDelete.emit(this.item);
  }

}
