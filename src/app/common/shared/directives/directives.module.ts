import { CommonModule } from '@angular/common';
import { AppCommonModule } from './../../app-common.module';
import { NgModule } from '@angular/core';
import { AccordionLinkDirective } from './accordion/accordionlink.directive';
import { AccordionDirective } from './accordion/accordion.directive';
import { AccordionAnchorDirective } from './accordion/accordionanchor.directive';
import { ParentRemoveDirective } from './elements/parent-remove.directive';
import { ToggleFullscreenDirective } from './fullscreen/toggle-fullscreen.directive';
import { SlimScroll } from './slim-scroll/index';
import { MaskerDirective } from './mask/brmasker.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        AccordionAnchorDirective,
        AccordionDirective,
        AccordionLinkDirective,
        ParentRemoveDirective,
        ToggleFullscreenDirective,
        SlimScroll,
        MaskerDirective
    ],
    declarations: [
        AccordionAnchorDirective,
        AccordionDirective,
        AccordionLinkDirective,
        ParentRemoveDirective,
        ToggleFullscreenDirective,
        SlimScroll,
        MaskerDirective
    ]
})
export class DirectivesModule { }