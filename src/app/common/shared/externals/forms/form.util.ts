import { Injectable } from "@angular/core";

@Injectable()
export class FormUtil {

    compareWith(valor1: any, valor2: any): boolean {
        if(valor1.id)
            return (valor2 !== undefined && valor2 !== null) ? valor1.id === valor2.id : false;
        else
            return (valor2 !== undefined) ? valor1 === valor2 : false;
    }
   
    /* compareByValue(valor1: any, valor2: any): boolean {
        return (valor2 !== undefined) ? valor1 === valor2 : false;
    } */
}