import { Input, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormComponent, IOption } from './../form.component';
import { Component, OnInit, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent extends FormComponent {
  
  @Input() modelList: boolean;
  @Input() list: IOption[];
  
  @ViewChild('content') content;
  showContent = false;

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  ngAfterViewInit() {
    this.showContent = this.content.nativeElement && this.content.nativeElement.children.length > 0;
    this.cdr.detectChanges();
  }
}
