import { FormComponent } from './../form.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { Component, OnInit, forwardRef, Input, EventEmitter, Output, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true
    }
  ]
})
export class InputComponent extends FormComponent {

  @Input() type: string;
  @Input() accept: string;
  @Input() tooltip: string;
  @Input() placementTooltip = 'top';
  @Input() placeholder: string;
  @Input() readonly: boolean;
  @Input() maxlength: string;
  @Input() minlength: string;
  @Input() max: string;
  @Input() min: string;

  @Output() validate = new EventEmitter<any>();

  constructor(cdr: ChangeDetectorRef) {
    super(cdr);
  }

  validateEvent() {
    this.validate.emit(this.control);
  }
}