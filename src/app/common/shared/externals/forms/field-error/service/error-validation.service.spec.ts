/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ErrorValidationService } from './error-validation.service';

describe('Service: ErrorValidation', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErrorValidationService]
    });
  });

  it('should ...', inject([ErrorValidationService], (service: ErrorValidationService) => {
    expect(service).toBeTruthy();
  }));
});