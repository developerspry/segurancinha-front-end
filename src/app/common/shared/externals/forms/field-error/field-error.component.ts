import { Subscription } from 'rxjs/Subscription';
import { ErrorValidation } from './../../../../http-interceptor/model/error-validation.model';
import { ErrorValidationService } from './service/error-validation.service';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgControl } from '@angular/forms';

@Component({
  selector: 'app-field-error',
  templateUrl: './field-error.component.html',
  styleUrls: ['./field-error.component.scss']
})
export class FieldErrorComponent implements OnInit {

  @Input() control: NgControl;
  @Input() formSubmitted: boolean;
  @Input() field: string;
  @Output() errorValidationEvent = new EventEmitter<any>();
  private errorsValidation: ErrorValidation[] = [];
  private subs: Subscription;
  public showErrorValidation = false;

  constructor(private  translate: TranslateService, private errorValidationService: ErrorValidationService) { }

  ngOnInit() {
    this.subs = this.errorValidationService.getErrorValidation()
      .subscribe(errosResponse => {
        if (errosResponse) {
          this.errorsValidation = [];
          errosResponse.forEach(error => {
            if (this.field === error.field) this.errorsValidation.push(error);
          });
          this.showErrorValidation = this.errorsValidation.length > 0;
          if (this.showErrorValidation) this.errorValidationEvent.emit(true);
        }
      });
  }

  showError(instant: boolean) {
    if (instant === undefined) {
      return this.formSubmitted;
    } else {
      return (!instant) ? this.formSubmitted : true;
    }
  }

  getErrorMessage(erroCode: string) {
    if (erroCode === 'required') erroCode = 'empty';
    const key = 'errors.' + this.control.name + '.' + erroCode;
    let errorLength;
    switch (erroCode) {
      case 'minlength':
        errorLength = this.control.getError('minlength');
        return this.translate.instant(key, { min: errorLength.requiredLength });
      case 'maxlength':
        errorLength = this.control.getError('maxlength');
        return this.translate.instant(key, { max: errorLength.requiredLength });
      default:
        return this.translate.instant(key);
    }
  }

  getMessageErrorValidation(code: string) {
    const key = 'errors.' + code;
    return this.translate.instant(key);
  }

}
