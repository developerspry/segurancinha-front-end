import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaskDirective } from './mask.directive';
import { MaskService } from './mask.service';

@NgModule({
  providers: [
    MaskService
  ],
  declarations: [
    MaskDirective
  ],
  exports: [
    MaskDirective
  ]
})
export class MaskModule {}
