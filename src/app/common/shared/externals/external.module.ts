import { MaskModule } from './mask/mask.module';
import { NgIfPermissionDirective } from './directives/ng-if-permission.directive';
import { SearchDirective } from './directives/search.directive';
import { MaskService } from './mask/mask.service';
import { SelectComponent } from './forms/select/select.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { KeysPipe } from './pipes/keys.pipe';
import { FieldErrorComponent } from './forms/field-error/field-error.component';
import { FormsModule } from '@angular/forms';
import { InputComponent } from './forms/input/input.component';
import { InputGroupComponent } from './forms/input-group/input-group.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { DateTimeFormatPipe } from './pipes/date-time-format.pipe';
import { Ng2BRPipesModule } from 'ng2-brpipes';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    MaskModule,
    Ng2BRPipesModule
  ],
  declarations: [
    FieldErrorComponent,
    InputGroupComponent,
    InputComponent,
    SelectComponent,
    KeysPipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    SearchDirective,
    NgIfPermissionDirective
  ],
  exports: [
    FieldErrorComponent,
    InputGroupComponent,
    InputComponent,
    SelectComponent,
    KeysPipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    SearchDirective,
    NgIfPermissionDirective,
    MaskModule,
    Ng2BRPipesModule
  ],
  providers: [MaskService]
})
export class ExternalModule { }