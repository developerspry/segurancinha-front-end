import { NgControl } from '@angular/forms';
import { Directive, ElementRef, HostListener, ViewChild, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[search]'
})
export class SearchDirective {

  @Output() searchRequest = new EventEmitter<any>();
  @Output() clickOut = new EventEmitter<any>();
  @Output() focusoutValidation = new EventEmitter<any>();

  focusOut = true;

  constructor(
    private _elementRef: ElementRef,
    private _control: NgControl) { }

  @HostListener('input', ['$event']) onInput(event: any) {
    let valueElement = '';
    if (this._elementRef.nativeElement.value) {
      valueElement = this._elementRef.nativeElement.value;
    } else {
      valueElement = event.target.value;
    }
    this.focusOut = true;
    if (valueElement.length > 0) {
      this.searchRequest.emit();
    } else {
      this.clickOut.emit();
    }
  }

  @HostListener('mousedown', ['$event']) onMousedown(event: any) {
    if ((<string> event.target.className).search('autocomplete-data') == 0) {
      this.focusOut = false;
    }
  }

  @HostListener('focusout', ['$event']) onFocusout(event: any) {
    if (this.focusOut) this.clickOut.emit();
    if (this._control.control) this.focusoutValidation.emit(this._control.control);
  }

}
