import { Injectable } from '@angular/core';

@Injectable()
export class TranslateGroupService {
    
    private translateGroup: string;
    
    constructor(group: string = "") {
        this.translateGroup = group;
    }

    get group(){
        return this.translateGroup;
    }
}