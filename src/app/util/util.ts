import { Injectable } from "@angular/core";

@Injectable()
export class Util {

    dateToString(value: any, separator = '-') {
        if (value) {
            let day = value.getDate() + '';
            if (day.toString().length === 1) {
                day = '0' + day;
            }
            let month = (value.getMonth() + 1) + '';
            if (month.toString().length === 1) {
                month = '0' + month;
            }
            const year = value.getFullYear();

            return day + separator + month + separator + year;
        }
        return value;
    }

    getDirectoryImageByExtension(extension:string, height:number):string{
        return "assets/icon/file-extension/"+height+"/"+extension+".png";
    }

    download(dados: any, name:string) {
        var byteCharacters = atob(dados);

        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
            byteNumbers[i] = byteCharacters.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        var blob = new Blob([byteArray], { type: 'application/octet-stream' });

        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(blob, name);
        } else {
            const a = document.createElement('a');
            document.body.appendChild(a);
            const url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = name;
            a.click();
            setTimeout(() => {
            window.URL.revokeObjectURL(url);
            document.body.removeChild(a);
            }, 0)
        }
    }
    

}