import { RouterModule } from '@angular/router';
import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersFormComponent } from './users-form/users-form.component';
import { UsersListComponent } from './users-list/users-list.component';
import { usersRoutes } from './users.routing';
import { UserService } from './service/user.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { Ng2BRPipesModule } from 'ng2-brpipes';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(usersRoutes),
    NgxSmartModalModule.forRoot()
  ],
  declarations: [
    UsersFormComponent,
    UsersListComponent
  ],
  providers: [UserService]
})
export class UsersModule { }