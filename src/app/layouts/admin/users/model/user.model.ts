import { Basic } from '../../models/basic.model';

export class User extends Basic{
    name: string;
    login: string;
    email: string;
    phone: string;
    profileType: string;
}