import { UsersListComponent } from './users-list/users-list.component';
import { UsersFormComponent } from './users-form/users-form.component';
import { Routes } from '@angular/router';

export const usersRoutes: Routes = [
    {
        path: '', component: UsersListComponent,
        data: {
            breadcrumb: 'breadcrumb.users'
        }
    },
    {
        path: 'edit/:id', component: UsersFormComponent, data: {
            breadcrumb: 'breadcrumb.userEdit'
        }
    },
    {
        path: 'new', component: UsersFormComponent, data: {
            breadcrumb: 'breadcrumb.userNew'
        }
    }
];