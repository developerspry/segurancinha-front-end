import { User } from './../model/user.model';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Injectable()
export class UserService {

    constructor(private http: HttpClient) { }

    insert(user: User) {
        return this.http.post<User>(`${environment.url_application}/user/`, user).toPromise();
    }

    update(user: User) {
        return this.http.put<User>(`${environment.url_application}/user/`, user).toPromise();
    }
  
    delete(user: User) {
      return this.http.delete<User>(`${environment.url_application}/user/${user.id}`).toPromise();
    }

    getAll() {
        return this.http.get<User[]>(`${environment.url_application}/user/`).toPromise();
    }

    findById(id: number) {
        return this.http.get<User>(`${environment.url_application}/user/${id}`).toPromise();
    }

}