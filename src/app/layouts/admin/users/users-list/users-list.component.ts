import { Component, OnInit } from '@angular/core';
import { UserService } from '../service/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { List } from '../../list';
import { TranslateService } from '@ngx-translate/core';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS } from '../../../../util/ConstantProject';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { User } from '../model/user.model';
import { AuthenticationService } from '../../../../common/authentication/authentication.service';
import { LoggedUser } from '../../../../common/authentication/logged-user';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent extends List implements OnInit {

  dataProvider = [];
  selectedUser: User = new User();
  user: LoggedUser = new LoggedUser();

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    private userService: UserService,
    public alertCenterService: AlertCenterService,
    public ngxSmartModalService: NgxSmartModalService,
    private authService: AuthenticationService) {
    super(translate, router, activeRoute, "users", alertCenterService, ngxSmartModalService);
  }

  ngOnInit() {
    this.user = this.authService.getLoggedUser();
    this.loadDataProvider();
  }

  loadDataProvider() {
    this.dataProvider = [];

    if (this.user.profileType == 'GERENTE') {
      this.userService.getAll().then((user) => this.dataProvider.push(user));
    } else {
      let users = [];
      this.userService.findById(this.user.id).then((user) => users.push(user));
      this.dataProvider[0] = users;
    }
  }

  edit(item) {
    this.router.navigate([this.routeEdit + item.id]);
  }

  confirmDelete(item) {
    this.userService.delete(item).then(
      item => {
        this.loadDataProvider();
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalDetails(item) {
    this.selectedUser = item;
    this.openModal();
  }

  sameUser() {
    return this.user.id === this.selectedUser.id;
  }

}
