import { ErrorValidationService } from './../../../../common/shared/externals/forms/field-error/service/error-validation.service';
import { User } from './../model/user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Form } from '../../form';
import { IOption } from '../../../../common/shared/externals/forms/form.component';
import { UserService } from '../service/user.service';
import { AlertCenterService } from '../../../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_ERROR, MESSAGE_OPERATION_ERROR, TITLE_SUCCESS, TITLE_WARNING, MESSAGE_OPERATION_WARNING_PASSWORD_WRONG } from '../../../../util/ConstantProject';
import { AuthenticationService } from '../../../../common/authentication/authentication.service';
import { LoggedUser } from '../../../../common/authentication/logged-user';
import { AlertConfirmationPasswordComponent } from '../../../../common/shared/components/alert-confirmation-password/alert-confirmation-password.component';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss']
})
export class UsersFormComponent extends Form implements OnInit {

  @ViewChild(AlertConfirmationPasswordComponent) confirmationPasswordModal;

  constructor(public translate: TranslateService,
    public router: Router,
    public activeRoute: ActivatedRoute,
    public errorValidationService: ErrorValidationService,
    public alertCenterService: AlertCenterService,
    public userService: UserService,
    private authService: AuthenticationService) {
    super(translate, router, activeRoute, 'users', errorValidationService, alertCenterService);
  }

  user: User = new User;
  loggedUser: LoggedUser = new LoggedUser;
  listProfileTypes: IOption[] = [
    {
      id: 1,
      label: 'GERENTE',
      hasLabel: true,
      value: 'GERENTE'
    },
    {
      id: 2,
      label: 'COMUM',
      hasLabel: true,
      value: 'COMUM'
    }
  ];

  ngOnInit() {
    this.loggedUser = this.authService.getLoggedUser();
    if (!this.checkEdit()) {

    }
  }

  insert(): void {
    this.userService.insert(this.user).then(
      user => {
        this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  update(): void {
    this.userService.update(this.user).then(
      user => {
        this.callbackInsertOrUpdate();
      }, (response: Response) => {
        this.errorValidation(response);
      }
    );
  }

  findById(id: any) {
    return this.userService.findById(id).then(
      user => {
        this.user = user;
      },
      (error: Response) => {
        this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
        console.log(error);
      }
    );
  }

  openModalConfirmPassword(){
      this.confirmationPasswordModal.openModal(this.loggedUser);
  }

  confirmUpdate(event){
    if(event){
      this.confirmRegister();
    } else {
      this.alertCenterService.alertWarning(TITLE_WARNING, MESSAGE_OPERATION_WARNING_PASSWORD_WRONG);
    }
  }

}