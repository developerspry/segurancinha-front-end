import { MenuService } from './../../common/shared/components/menu/menu.service';
import { Menu } from './../../common/shared/components/menu/menu.model';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../common/authentication/authentication.service';
import { Component, OnInit, ViewChild, ViewEncapsulation, ElementRef, AfterViewInit, Input } from '@angular/core';
import 'rxjs/add/operator/filter';
import { state, style, transition, animate, trigger, AUTO_STYLE } from '@angular/animations';
import { TranslateService } from '@ngx-translate/core';

export interface Options {
  heading?: string;
  removeFooter?: boolean;
  mapHeader?: boolean;
}

@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('400ms ease-in-out')
      ])
    ])
  ]
})

export class AdminLayoutComponent implements OnInit {
  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  isCollapsedSideBar = 'no-block';
  toggleOn = true;
  windowWidth: number;
  menuItems: Menu[];

  userProfile: string;

  languages = [{
    id: 'pt',
    name: 'Português',
    srcIconBar: 'assets/icon/countries/32/pt.png',
    srcIconDropdown: 'assets/icon/countries/32/pt.png'
  },{
    id: 'es',
    name: 'Español',
    srcIconBar: 'assets/icon/countries/32/es.png',
    srcIconDropdown: 'assets/icon/countries/32/es.png'
  },{
    id: 'en',
    name: 'English',
    srcIconBar: 'assets/icon/countries/32/en.png',
    srcIconDropdown: 'assets/icon/countries/32/en.png'
  }];

  language;

  constructor(public menuItemsService: MenuService,
      private router: Router,
      private authService: AuthenticationService,
      public translate: TranslateService) {
    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);

    this.language = this.languages.filter(language => language.id == translate.store.currentLang)[0];
  }

  ngOnInit() { 
    this.menuItems = this.menuItemsService.getMenu();
    this.userProfile = this.authService.getLoggedUser().name;
  }

  onLogout(){
    this.authService.logout();
  }

  onClickedOutside(e: Event) {
    if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
      this.toggleOn = true;
      this.verticalNavType = 'offcanvas';
    }
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    let reSizeFlag = true;
    if (this.deviceType === 'tablet' && this.windowWidth >= 768 && this.windowWidth <= 1024) {
      reSizeFlag = false;
    } else if (this.deviceType === 'mobile' && this.windowWidth < 768) {
      reSizeFlag = false;
    }

    if (reSizeFlag) {
      this.setMenuAttributs(this.windowWidth);
    }
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
      this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
      this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
    } else {
      this.verticalNavType = this.verticalNavType === 'expanded' ? 'collapsed' : 'expanded';
    }
  }

  toggleOpenedSidebar() {
    this.isCollapsedSideBar = this.isCollapsedSideBar === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  changeLanguage(languageParam: string){
    this.translate.use(languageParam);
    this.language = this.languages.filter(language => language.id == languageParam)[0];
    // this.router.navigate(["/"]);
  }

  getDirectoryImageByExtension(language:string, height:number):string{
    return "assets/icon/countries/"+height+"/"+language+".png";
  }

}
