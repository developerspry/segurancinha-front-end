import { ErrorValidationService } from './../../common/shared/externals/forms/field-error/service/error-validation.service';
import { IOption } from './../../common/shared/externals/forms/form.component';
import { ErrorValidation } from './../../common/http-interceptor/model/error-validation.model';
import { ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AlertCenterService } from '../../common/shared/components/alert-center/service/alert-center.service';
import { TITLE_SUCCESS, TITLE_ERROR, MESSAGE_OPERATION_ERROR, MESSAGE_OPERATION_SUCCESS } from '../../util/ConstantProject';

export abstract class Form {

    constructor(public translate: TranslateService,
        public router: Router,
        public activeRoute: ActivatedRoute,
        public route: string,
        public errorValidationService: ErrorValidationService,
        public alertCenterService: AlertCenterService) { }

    modelEdit = false;
    idEdit = 0;
    @ViewChild('form') public form: NgForm;

    public routeEdit: String = '/' + this.route + '/edit/';

    abstract ngOnInit(): void;
    abstract insert(): void;
    abstract update(): void;
    abstract findById(id): any;
    abstract openModalConfirmPassword(): any;

    submit(formValido: boolean) {
        if (formValido) {
            this.openModalConfirmPassword();
        }
    }

    confirmRegister() {
        if (!this.modelEdit) {
            this.insert();
        } else {
            this.update();
        }
    }

    callbackInsertOrUpdate(): void {
        this.alertCenterService.alertSuccess(TITLE_SUCCESS, MESSAGE_OPERATION_SUCCESS);
        this.comeback();
    }

    comeback(args?: any) {
        this.router.navigate([this.route + '/'], {
            queryParams: {
                data: args
            }
        });
    }

    checkEdit() {
        this.activeRoute.params.subscribe(params => {
            const id = params['id'];
            if (id !== undefined) {
                this.findById(id);
                this.modelEdit = true;
                this.idEdit = id;
                return true;
            }
        });
        return false;
    }

    addError(valid, control, key: string, instant = false) {
        if (!valid) {
            this.form.controls[control].setErrors(
                {
                    [key]: { instant: [instant] }
                }
            );
        }
    }

    removeErrors(control) {
        if (this.form.controls[control]) { this.form.controls[control].setErrors(null); }
    }

    idModel(model: any) {
        let id = 0;
        if (model.id !== undefined) {
            id = model.id;
        }
        return id;
    }

    unmask(value: string) {
        /* if (this.formUtil.emptyOrUndefined(valor)) {
          return;
        } */
        return value.replace(/\D+/g, '');
    }

    errorValidation(response) {
        if (response.status === 422) {
            const errorsValidation: ErrorValidation[] = [];
            const errors: ErrorValidation[] = response.error;
            errors.forEach((error: ErrorValidation) => {
                if (error.field) { errorsValidation.push(error); }
            });
            if (errorsValidation.length > 0) { this.errorValidationService.setErrorsValidation(errorsValidation); }
        } else {
            this.alertCenterService.alertDanger(TITLE_ERROR, MESSAGE_OPERATION_ERROR);
            console.log(response);
        }
    }

    modelListToIOptions(modelList: any[], label: string): IOption[] {
        const listOptions: IOption[] = [];
        modelList.forEach(item => {
            const option: IOption = {
                value: item,
                id: (item.id) ? item.id : undefined,
                label: item[label],
                hasLabel: label !== undefined
            };
            listOptions.push(option);
        });

        return listOptions;
    }
}
