import { AppCommonModule } from './../../../common/app-common.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { homeRoutes } from './home.routing';

@NgModule({
  imports: [
    AppCommonModule,
    RouterModule.forChild(homeRoutes),
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
