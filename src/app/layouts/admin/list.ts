import { ErrorValidationService } from './../../common/shared/externals/forms/field-error/service/error-validation.service';
import { Util } from './../../util/util';
import { IOption } from './../../common/shared/externals/forms/form.component';
import { ErrorValidation } from './../../common/http-interceptor/model/error-validation.model';
import { ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AlertCenterService } from '../../common/shared/components/alert-center/service/alert-center.service';
import { AlertConfirmationDeleteComponent } from '../../common/shared/components/alert-confirmation-delete/alert-confirmation-delete.component';
import { NgxSmartModalService } from 'ngx-smart-modal';

export abstract class List {

    @ViewChild(AlertConfirmationDeleteComponent) deleteModal;

    constructor(public translate: TranslateService,
        public router: Router,
        public activeRoute: ActivatedRoute,
        public route: string,
        public alertCenterService:AlertCenterService,
        public ngxSmartModalService: NgxSmartModalService) { }

    public routeEdit:String = "/"+this.route+"/edit/";

    util: Util = new Util();

    abstract ngOnInit(): void;
    abstract loadDataProvider(): void;
    abstract edit(item): void;
    abstract confirmDelete(item): void;

    openModal(){
      this.ngxSmartModalService.getModal('modalDetail').open();
    }

    delete(item){
        this.deleteModal.openModal(item);
    }

}