import { User } from "../users/model/user.model";

export class Basic {
    id: number;
    dateInsert?: any;
    dateUpdate?: any;
    userInsert?: User;
    userUpdate?: User;
}