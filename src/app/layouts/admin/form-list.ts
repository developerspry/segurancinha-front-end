import { ErrorValidationService } from './../../common/shared/externals/forms/field-error/service/error-validation.service';
import { Util } from './../../util/util';
import { IOption } from './../../common/shared/externals/forms/form.component';
import { ErrorValidation } from './../../common/http-interceptor/model/error-validation.model';
import { ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AlertCenterService } from '../../common/shared/components/alert-center/service/alert-center.service';
import { AlertConfirmationDeleteComponent } from '../../common/shared/components/alert-confirmation-delete/alert-confirmation-delete.component';
import { Form } from './form';
import { NgxSmartModalService } from 'ngx-smart-modal';

export abstract class FormList extends Form {

    @ViewChild(AlertConfirmationDeleteComponent) deleteModal;

    constructor(public translate: TranslateService,
        public router: Router,
        public activeRoute: ActivatedRoute,
        public route: string,
        public errorValidationService: ErrorValidationService,
        public alertCenterService:AlertCenterService,
        public ngxSmartModalService: NgxSmartModalService) {
        super(translate, router, activeRoute, route, errorValidationService, alertCenterService);
    }

    abstract confirmDelete(item): void;

    openModal(){
      this.ngxSmartModalService.getModal('modalDetail').open();
    }

    delete(item){
        this.deleteModal.openModal(item);
    }

}