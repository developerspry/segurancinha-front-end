import { AdminLayoutComponent } from './admin-layout.component';
import { Routes } from '@angular/router';

export const AdminLayoutRoutes: Routes = [
    {
        path: '', component: AdminLayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full'
            }, {
                path: 'home',
                loadChildren: './home/home.module#HomeModule'
            },
            {
                path: 'users',
                loadChildren: './users/users.module#UsersModule'
            }
        ]
    }
];
