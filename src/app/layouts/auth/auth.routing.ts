import { LockScreenComponent } from './lock-screen/lock-screen.component';
import { ForgotComponent } from './forgot/forgot.component';

import { Routes } from '@angular/router';

export const AuthRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'login',
        loadChildren: './login/login.module#LoginModule',
        data: {
          breadcrumb: 'Login'
        }
      },
      {
        path: 'forgot',
        component: ForgotComponent,
        data: {
          breadcrumb: 'Forgot'
        }
      }]
  }
];
