import { Credential } from './credential.model';
import { AuthenticationService } from './../../../common/authentication/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AlertCenterService } from '../../../common/shared/components/alert-center/service/alert-center.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credential: Credential = new Credential();
  isShowPassword: boolean = false;

  constructor(private router: Router,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private alertCenterService: AlertCenterService) { }

  ngOnInit() {
  }

  onAuthenticate(validForm) {
    if (validForm) {
      this.authService.authentication(this.credential.login, this.credential.password)
        .then(resp => {
          resp.body.token = resp.headers.get('x-auth-token');
          
          console.log(resp.body);
          
          this.authService.setLoggedUser(resp.body);
          this.alertCenterService.alertSuccess("alert.welcome", this.authService.getLoggedUser().name);
          this.router.navigate(['/']);
        }).catch(error => {
          console.error('[' + error.error + '] - ' + error.message);
        });
    }
  }

  showPassword(){
    this.isShowPassword = !this.isShowPassword;
  }

}