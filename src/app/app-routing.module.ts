import { AuthGuard } from './common/guards/auth.guard';
import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    loadChildren: './layouts/admin/admin-layout.module#AdminLayoutModule',
    canActivate: [AuthGuard]
  }, {
    path: 'authentication',
    loadChildren: './layouts/auth/auth.module#AuthModule'
  }, {
    path: 'not-found',
    loadChildren: './not-found/not-found.module#NotFoundModule'
  },
  { path: '**', redirectTo: 'not-found' } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
