import { NotFoundComponent } from './not-found.component';
import { Routes } from '@angular/router';

export const NotFoundRoutes: Routes = [{
  path: '',
  component: NotFoundComponent,
  data: {
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
